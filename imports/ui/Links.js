import { Meteor } from 'meteor/meteor';
import React from 'react';
import { Session } from 'meteor/session';
import { Tracker } from 'meteor/tracker';

import { LinkListItem } from './LinkListItem';
import { LinksAPI } from '../api/links';

export class Links extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            links: []
        };
        this.renderLinks = this.renderLinks.bind(this);
    }

    componentDidMount() {
        Meteor.subscribe('links');
        this.trackerAutorun = Tracker.autorun(() => {
            const showAll = Session.get('showAll');
            let linksQuery;
            if (showAll) {
                linksQuery = {};
            } else {
                linksQuery = { visible: true };
            }

            const links = LinksAPI.find(linksQuery).fetch();
            this.setState({ links });
        });
    }

    componentWillUnmount() {
        this.trackerAutorun.stop();
    }

    render() {
        return (
            <ul>
                { this.renderLinks() }
            </ul>
        )
    }

    renderLinks() {
        return this.state.links.map((link) => {
            const shortURL = Meteor.absoluteUrl(link._id);
            link.shortURL = shortURL;
            return (
                <li key={ link._id }>
                    <LinkListItem link={ link } />
                </li>
            )
        });
    }
}