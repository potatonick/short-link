import PropTypes from 'prop-types';
import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import { Link } from './Link';
import { Login } from './Login';
import { NotFound } from './NotFound';
import { Signup } from './Signup';

export class App extends React.Component {
    render() {
        return (
            <Router history={this.props.history}>
                <Switch>
                    <Route exact path="/" component={Login} />
                    <Route path="/signup" component={Signup} />
                    <Route path="/links" component={Link} />
                    <Route path="*" component={NotFound} />
                </Switch>
            </Router>
        );
    }
}

App.propTypes = {
    history: PropTypes.object.isRequired
};