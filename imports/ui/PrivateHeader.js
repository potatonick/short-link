import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import React from 'react';

export class PrivateHeader extends React.Component {
    constructor(props) {
        super(props);
        this.signOut = this.signOut.bind(this);
    }

    render() {
        return (
            <div>
                <h1>{ this.props.title }</h1>
                <button onClick={this.signOut}>Sign Out</button>
            </div>
        );
    }

    signOut() {
        Meteor.logout();
    }
}

PrivateHeader.propTypes = {
    title: PropTypes.string.isRequired
};