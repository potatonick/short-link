import { Accounts } from 'meteor/accounts-base';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import React from 'react';

export class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        const isAuthorized = !! Meteor.userId();
        if (isAuthorized) {
            this.props.history.replace('/links');
        }
    }

    render() {
        return (
            <div>
                <h1>Sign Up</h1>
                {this.state.error ? <p>{this.state.error}</p> : undefined}
                <form onSubmit={this.handleSubmit}>
                    <input type="email" ref="email" name="email" placeholder="Email" />
                    <input type="password" ref="password" name="password" placeholder="Password" />
                    <input type="submit" value="Create Account" />
                </form>
                <p>Already have an account? <Link to="/">Log In</Link></p>
            </div>
        );
    }

    handleSubmit(e) {
        e.preventDefault();
        const email = this.refs.email.value;
        const password = this.refs.password.value;

        if (password.length < 6) {
            this.setState({ error: 'Password must be at least 6 characters long' });
            return;
        }

        Accounts.createUser({ email, password }, (err) => {
            const reason = err ? err.reason : '';
            this.setState({ error: reason });
        });
    }
}