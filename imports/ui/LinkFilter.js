import { Tracker } from 'meteor/tracker';
import { Session } from 'meteor/session';
import React from 'react';

export class LinkFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showAll: false };
    }

    componentDidMount() {
        Session.set('showAll', false);
        this.tracker = Tracker.autorun(() => {
            const showAll = Session.get('showAll');
            this.setState({ showAll });
        });
    }

    componentWillUnmount() {
        this.tracker.stop();
    }

    render() {
        return (
            <div>
                <label htmlFor={"links-show-all"}>Show all</label>
                <input
                    id="links-show-all"
                    type="checkbox"
                    checked={this.state.showAll}
                    onChange={
                        (e) => {
                            const checked = e.target.checked;
                            Session.set('showAll', checked);
                        }
                    }
                />
            </div>
        )
    }
}