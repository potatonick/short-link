import { Meteor } from 'meteor/meteor';
import React from 'react';

import { AddLink } from './AddLink';
import { Links } from './Links';
import { LinkFilter } from './LinkFilter';
import { PrivateHeader } from './PrivateHeader';

export class Link extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const isAuthorized = !! Meteor.userId();
        if (! isAuthorized) {
            this.props.history.replace('/');
        }
    }

    render() {
        return (
            <div>
                <PrivateHeader title="Saved links are shown here" />
                <LinkFilter />
                <AddLink />
                <Links />
            </div>
        );
    }
}