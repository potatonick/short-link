import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import React from 'react';

export class LinkListItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            copyButtonText: 'Copy'
        };

        this.onCopy = this.onCopy.bind(this);
        this.toggleVisibility = this.toggleVisibility.bind(this);
    }

    render() {
        return (
            <div>
                <label htmlFor={ this.props.link._id }>Short URL</label>
                <input
                    id={ this.props.link._id }
                    ref="inputShortURL"
                    readOnly={ true }
                    type="text"
                    value={ this.props.link.shortURL }
                />
                <button onClick={ this.onCopy }>{ this.state.copyButtonText }</button>

                <p>{ this.props.link.url }</p>
                <p>{ `Visited ${ this.props.link.visitCount } time(s)` }</p>
                <p>{ this.displayLastVisited() }</p>

                <button onClick={ this.toggleVisibility }>
                    { this.props.link.visible ? 'Hide' : 'Show' }
                </button>
            </div>
        );
    }

    onCopy() {
        this.refs.inputShortURL.select();
        let text = '';
        if (document.execCommand('copy')) {
            text = 'Copied';
        } else {
            text = 'Cannot copy';
        }

        this.setState({ copyButtonText: text });
        setTimeout(() => {
            this.setState({ copyButtonText: 'Copy' });
        }, 1000);
    }

    toggleVisibility() {
        Meteor.call('links.toggleVisibility', this.props.link._id);
    }

    displayLastVisited() {
        const lastVisit = this.props.link.lastVisited;
        if (lastVisit) {
            const visitDate = new Date(lastVisit);
            return visitDate.toLocaleDateString();
        } else {
            return 'Never visited';
        }
    }
}

LinkListItem.propTypes = {
    link: PropTypes.object.isRequired
};