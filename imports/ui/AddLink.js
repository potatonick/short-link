import { Meteor } from 'meteor/meteor';
import Modal from 'react-modal';
import React from 'react';

export class AddLink extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showDialog: false,
            errorMessage: ''
        };

        this.onAddLink = this.onAddLink.bind(this);
        this.handleDialogClose = this.handleDialogClose.bind(this);
        this.showDialog = this.showDialog.bind(this);
    }

    componentDidMount() {
        Modal.setAppElement('#app');
    }

    render() {
        return (
            <div>
                <button onClick={ this.showDialog }>+ Add link</button>
                <Modal
                    isOpen={ this.state.showDialog }
                    contentLabel="Add link"
                    shouldCloseOnOverlayClick={ true }
                    onRequestClose={ this.handleDialogClose }>
                    <h1>Add link</h1>
                    <p>{ this.state.errorMessage }</p>
                    <form onSubmit={this.onAddLink}>
                        <input type="text" ref="url" placeholder="Link" />
                        <input type="submit" value="Add" />
                    </form>
                    <button onClick={ this.handleDialogClose }>Cancel</button>
                </Modal>
            </div>
        );
    }

    onAddLink(e) {
        e.preventDefault();
        const url = this.refs.url.value;

        if (url) {
            Meteor.call('link.insert', url, (err, res) => {
                if (err) {
                    this.setState({ errorMessage: err.reason });
                } else {
                    this.handleDialogClose();
                }
            });
        }
    }

    handleDialogClose() {
        this.setState({
            showDialog: false,
            errorMessage: ''
        });
    }

    showDialog() {
        this.setState({ showDialog: true });
    }
}