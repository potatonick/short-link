const authenticatedPages = ['/links'];
const unauthenticatedPages = ['/', '/signup'];

export function redirectUser(isAuthenticated, history) {
    const path = history.location.pathname;
    const isInAuthenticatedPage = authenticatedPages.includes(path);
    const isInUnauthenticatedPage = unauthenticatedPages.includes(path);

    if (isAuthenticated && isInUnauthenticatedPage) {
        history.replace('/links');
    } else if (!isAuthenticated && isInAuthenticatedPage) {
        history.replace('/');
    }
}