import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const urlSchema = new SimpleSchema({
    url: {
        type: String,
        regEx: SimpleSchema.RegEx.Url
    },
    userId: { type: String },
    visible: { type: Boolean },
    visitCount: { type: Number },
    lastVisited: {
        type: Date,
        required: false
    }
});
export const LinksAPI = new Mongo.Collection('links');

if (Meteor.isServer) {
    Meteor.publish('links', function () {
        return LinksAPI.find({ userId: this.userId });
    });
}

Meteor.methods({
    'link.insert'(url) {
        const userId = this.userId;
        if (! userId) {
            throw new Meteor.Error('LinkInsertError', 'Access denied');
        }

        const insertLink = {
            url,
            userId,
            visible: true,
            visitCount: 0,
            lastVisited: null
        };
        urlSchema.validate(insertLink); // If validation checks don't pass it won't get past here (will throw error)
        LinksAPI.insert(insertLink);
    },
    'links.toggleVisibility'(linkID) {
        const userId = this.userId;
        if (! userId) {
            throw new Meteor.Error('LinkModifyError', 'Cannot modify link when not logged in');
        }

        if (! linkID) {
            throw new Meteor.Error('LinkModifyError', 'Must pass link ID as argument');
        }

        const link = LinksAPI.findOne({ _id: linkID });
        if (! link) {
            throw new Meteor.Error('LinkModifyError', `Link with ID ${linkID} not found`);
        }

        const visible = ! link.visible;
        LinksAPI.update({ _id: linkID }, { $set: { visible } });
    }
});