import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';

const emailSchema = new SimpleSchema({
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    }
});

Meteor.startup(() => {
    Accounts.validateNewUser((user) => {
        const email = user.emails[0].address;
        emailSchema.validate({ email }); // If validation checks don't pass it won't get past here (will throw error)
        return true;
    });
});