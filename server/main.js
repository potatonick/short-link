import { Meteor } from 'meteor/meteor';
import { WebApp } from 'meteor/webapp';

import '../imports/api/server/accountValidator';
import  { LinksAPI } from '../imports/api/links';
import '../imports/startup/startup_setup';

Meteor.startup(() => {
    WebApp.connectHandlers.use((req, res, next) => {
        const queryID = req.url.slice(1);
        const link = LinksAPI.findOne({ _id: queryID });

        if (link) {
            res.statusCode = 302;
            res.setHeader('Location', link.url);

            // Update visit count
            // FIXME: validate new data with schemas
            LinksAPI.update({ _id: queryID }, { $set: {
                visitCount: link.visitCount + 1,
                lastVisited: new Date()
            } });
        }

        next();
    });
});