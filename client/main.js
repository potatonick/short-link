import createBrowserHistory from 'history/createBrowserHistory';
import { Meteor } from 'meteor/meteor';
import React from 'react';
import ReactDOM from 'react-dom';
import { Tracker } from 'meteor/tracker';

import { App } from '../imports/ui/App';
import { LinksAPI } from '../imports/api/links';
import { redirectUser } from '../imports/api/client/pageRedirect';
import '../imports/startup/startup_setup';

const history = createBrowserHistory();

Tracker.autorun(() => {
    const isAuthenticated = !! Meteor.userId();
    redirectUser(isAuthenticated, history);
});

Meteor.startup(() => {
    ReactDOM.render(<App history={history} />, document.getElementById('app'));
});